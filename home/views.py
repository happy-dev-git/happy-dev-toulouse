from django.views.generic import CreateView, TemplateView
from home.forms import ContactForm
from home.models import InboundContact


class HomePageView(TemplateView):
    template_name = "home/homepage.html"
    extra_context = {"contact_form": ContactForm()}


class ContactView(CreateView):
    model = InboundContact
    form_class = ContactForm
    template_name = "home/contact_page.html"

    def get_context_data(self, **kwargs):
        """Rename 'form' as 'contact_form' to be consistent with home page."""
        kwargs = super().get_context_data(**kwargs)
        kwargs["contact_form"] = kwargs["form"]
        return kwargs

    def form_valid(self, form):
        """Change template to display success message."""
        self.object = form.save()
        self.object.send()
        self.template_name = "home/partials/contact_success.html"
        return self.render_to_response(self.get_context_data())

class TestView(TemplateView):
    template_name = "home/test.html"
