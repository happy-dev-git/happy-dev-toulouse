from django.db import models
from django.utils import timezone

from brevo.connectors import Brevo


class InboundContact(models.Model):
    email = models.EmailField()
    message = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)
    sent_date = models.DateTimeField(null=True)
    error = models.TextField(null=True)

    def __str__(self):
        return self.email

    def send(self):
        body_message = f'{self.message}\n\nExpéditeur: {self.email}'
        try:
            Brevo().send_contact_email(body_message)
            self.sent_date = timezone.now()
        except Exception as e:
            self.error = str(e)
        self.save()
