from django import forms
from .models import InboundContact


class ContactForm(forms.ModelForm):
    class Meta:
        model = InboundContact
        fields = ["email", "message"]
        widgets = {
            "email": forms.EmailInput(attrs={"placeholder": "jane@doe.com"}),
            "message": forms.Textarea(attrs={"placeholder": "Message..."}),
        }
        labels = {"email": False, "message": False}
