from django.urls import path

from home import views

app_name = "home"


urlpatterns = [
    path("", views.HomePageView.as_view(), name="home"),
    path("contact/", views.ContactView.as_view(), name="contact"),
    path("test/", views.TestView.as_view(), name="test"),
]
