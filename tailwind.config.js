/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./**/templates/**/*.{html,js}'],
  theme: {
    extend: {
      fontFamily: {
        bigjohn: ['"Big John"', "sans-serif"],
      },
      colors: {
        yellow: {
          50: '#fffaeb',
          100: '#fff2c6',
          200: '#FFDE70',  // HD yellow
          300: '#ffcf4a',
          400: '#ffba20',
          500: '#ffba20',
          600: '#dd7002',
          700: '#b74e06',
          800: '#943b0c',
          900: '#7a310d',
          950: '#461802',
        },
        purple: {
          50: '#f0f3fd',
          100: '#e4e9fb',
          200: '#ced6f7',
          300: '#b0bbf1',
          400: '#9099e9',
          500: '#7577df',
          600: '#6760d2',  // HD purple
          700: '#544bb7',
          800: '#443f94',
          900: '#3b3976',
          950: '#232145',
        },
        success: {
          50: '#ebfef4',
          100: '#cffce4',
          200: '#a2f8cd',
          300: '#7ff1c0',  // original mint
          400: '#2add96',
          500: '#06c37e',
          600: '#009f67',
          700: '#007f56',
          800: '#016445',
          900: '#02523a',
          950: '#002f21',
        },
        danger: {
          50: '#fff3f1',
          100: '#ffe6e1',
          200: '#ffd1c7',
          300: '#ffb1a0',
          400: '#ff8065',  // original orange
          500: '#f85c3b',
          600: '#e5401d',
          700: '#c13214',
          800: '#a02d14',
          900: '#842b18',
          950: '#481207',
        }
      }
    },
  },
  plugins: [],
}

