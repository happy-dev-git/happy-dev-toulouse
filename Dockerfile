FROM python:3.11

LABEL maintainer="swann@toulouse.happy-dev.fr"
LABEL vendor="Happy Dev Toulouse"

# install poetry
ENV POETRY_HOME="/opt/poetry" \
    POETRY_NO_INTERACTION=1 \
    POETRY_VERSION=1.3.2 \
    POETRY_VIRTUALENVS_CREATE=0
ENV PATH="$POETRY_HOME/bin:$PATH"

WORKDIR /app

COPY install-poetry.py ./
RUN python install-poetry.py
RUN pip install debugpy -t /tmp

# install project dependencies
COPY poetry.lock pyproject.toml ./
RUN poetry config virtualenvs.create false
RUN poetry install --no-interaction --no-ansi

COPY . ./

EXPOSE 8080

# run webpack in watch mode and start the Python server
CMD [ "python", "manage.py", "runserver", "0.0.0.0:8080" ]
