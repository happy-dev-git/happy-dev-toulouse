# happy-dev-toulouse

## Getting started

1. copy past .env.example to .env, update content to match your configuration
2. run app in docker with: `docker compose up -d`

## When pulling

1. If required (missing python deps): rebuild container: `docker compose up -d --build` 
2. In docker container 'app', update database by running: `python manage.py migrate`

## When pushing to scalingo

poetry export -f requirements.txt --output requirements.txt

## Using tailwind

To build css output of tailwind, use `npx tailwindcss -i ./static/css/tailwind_input.css -o ./static/css/tailwind_output.css --watch`