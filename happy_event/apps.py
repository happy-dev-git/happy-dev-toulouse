from django.apps import AppConfig


class HappyEventConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'happy_event'
