from django.db import models


class UuidMixin(models.Model):
    public_id = models.UUIDField()