const body = document.querySelector('body')
const header = document.querySelector('header')
const tabs = document.querySelector(".tabs")

const toggleMenu = body.querySelector('#togglemenu')
const popout = body.querySelector('#popout')


document.addEventListener('DOMContentLoaded', () => {

    // Set margin top of <main> depending on header height
    setmainMarginTop()
    window.addEventListener('resize', setmainMarginTop, true) 


    /**
     * Mobile / Burger
     */

    toggleMenu.addEventListener("click", function(e) {
        e.stopPropagation();

        if(this.classList.contains('active')){
            popout.classList.remove('opened')
            this.classList.remove('active')
            this.setAttribute( 'aria-expanded', 'false' )
        }
        else{ 
            this.classList.add('active')
            this.setAttribute( 'aria-expanded', 'true' )
            popout.classList.add('opened')
        }
    })


    /**
     * Change contact href in main menu  (Home)
     */
    if (body.classList.contains('home')) {
         
        let datahref = body.querySelector('.contact').getAttribute('data-href')
        let href = body.querySelector('.contact').getAttribute('href')

        body.querySelector('.contact').setAttribute('href', datahref)
        body.querySelector('.contact').setAttribute('data-href', href)
        console.log('home2'+href)

    }


    /**
     * Soft scroll on Anchor links click
     */
    bodyLinks = body.querySelectorAll('a[href*="#"]')

    for(let i = 0; i<bodyLinks.length; i++) {

        bodyLinks[i].addEventListener("click", function(e) {

            //split the url by # to get the anchor target name
            let parts = this.href.split("#")
            let destination = parts[1]        
            //console.log(destination)

            // go to that anchor
            if(!!body.querySelector('#'+destination)){

                e.preventDefault(); 

                let destTop = body.querySelector('#'+destination).offsetTop
                //console.log('destTop1 ='+destTop);

                // If mobile we close the menu before
                if ( ! isHidden(toggleMenu) ) {
                    toggleMenu.click()
                }

               window.scrollTo({top: destTop-100, behavior: 'smooth'})

            }	     
        })
    }


    /* 
    * Animate on scroll
    */
    const scrollOffset = 0;
    const scrollElements = document.querySelectorAll(".m_scroll");

    if (scrollElements !== null) {

        const elementInView = (el, dividend = 1) => {
            const elementTop = el.getBoundingClientRect().top;
            return (
                elementTop <=
                (window.innerHeight || document.documentElement.clientHeight) / dividend
            );
        };

        const elementOutofView = (el) => {
            const elementTop = el.getBoundingClientRect().top;
            return (
                elementTop > (window.innerHeight || document.documentElement.clientHeight)
            );
        };

        const handleScrollAnimation = () => {
        scrollElements.forEach((el) => {
            if (elementInView(el, 1.1)) {
                el.classList.add("scrolled")
            } else if (elementOutofView(el)) {
                el.classList.remove("scrolled")
            }
        })
        }

        window.addEventListener("scroll", () => { 
            throttle(() => {
                handleScrollAnimation();
            }, 250);
        });

        // For elements in viewport on page load
        scrollElements.forEach((el) => {
            if (elementInView(el, 1)) {
                el.classList.add("scrolled")
            }
        })
        
    }
    // else{
    //     console.log('No js_scroll el in the page')
    // }



})


/*
* Throttle function to limit number of events
*/
 var throttleTimer = false;

 const throttle = (callback, time) => {
    //don't run the function while throttle timer is true
    if (throttleTimer) return;
    //first set throttle timer to true so the function doesn't run
    throttleTimer = true;

    setTimeout(() => {
        //call the callback function in the setTimeout and set the throttle timer to false after the indicated time has passed
        callback();
        throttleTimer = false;
    }, time);
}



// Set tabs'height based on active tab height
function setTabsHeight() {
    
    let activeTab = tabs.querySelector(".active").querySelector('[id^="tab"]').nextElementSibling.nextElementSibling
         
    // Desktop
    if ( isHidden(toggleMenu) ) {
        tabs.style.height = activeTab.offsetHeight+40+'px'
        //console.log('Hidden setTabsHeight '+(tabs.style.height))
    }
    // If mobile
    else{
        tabs.style.height = activeTab.offsetHeight+340+'px'
        //console.log('setTabsHeight '+(tabs.style.height))
    }
}

// Set main's margin-top based on header's height
function setmainMarginTop() {

    let main = document.querySelector('main')
    main.style.marginTop = header.offsetHeight+40+'px'
    //console.log('main margin = '+header.offsetHeight)
}

// Check if el is hidden in DOM
function isHidden(el) {
    return !(el.offsetWidth || el.offsetHeight || el.getClientRects().length);
}