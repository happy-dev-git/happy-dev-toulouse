from urllib import request
import requests
from django.conf import settings


class Brevo:
    def __init__(self):
        self.api_key = settings.BREVO_API_KEY
    
    def send_contact_email(self, BodyMessage):
        url = "https://api.brevo.com/v3/smtp/email"
        headers = {
            "accept": "application/json",
            "content-type": "application/json",
            "api-key": self.api_key,
        }
        data = {
            "templateId": 6,
            "params": {
                "bodyMessage": BodyMessage,
            },
            "to": [
                {
                    "email": "swann.bouviermuller@gmail.com",
                    "name": "Swann",
                },
            ],
        }
        response = requests.post(url, headers=headers, json=data)
        response.raise_for_status()
        return response.json()
