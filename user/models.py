from django.contrib.auth.models import AbstractUser
from django.db import models

from user.managers import UserManager


class User(AbstractUser):
    """User model."""

    username = None  # type: ignore

    email = models.EmailField("E-mail", unique=True)
    phone = models.CharField("Téléphone", max_length=25)

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    objects = UserManager()  # type: ignore

    def __str__(self):
        return self.email

    @property
    def initials(self):
        """Return the initials of the user: first letters of first and last name."""
        try:
            return (f"{self.first_name[0]}{self.last_name[0]}").upper()
        except IndexError:
            return ""

    def get_prefixed_phone(self) -> str:
        """Return the phone number with +33 prefix if not already prefixed."""
        if not self.phone:
            return ""
        if self.phone.startswith("+33"):
            return self.phone
        # remove prefixing 0
        return f"+33{self.phone[1:]}"

    class Meta:
        ordering = ["last_name", "first_name"]
        verbose_name = "Utilisateur"
