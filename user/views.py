"""
Views
"""
from datetime import datetime, timedelta

from django.contrib import messages
from django.contrib.auth import login as auth_login
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import (  # type: ignore
    LoginView,
    PasswordChangeView,
    RedirectURLMixin,
)
from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from django.views.generic import FormView, RedirectView, UpdateView

from user.forms import DoubleAuthCodeForm
from user.models import CodeExpiredException, User

DATETIME_FORMAT = "%Y%m%d%H%M%S"


class ChronoMixin:
    @staticmethod
    def start_chrono(request):
        start = datetime.now().strftime(DATETIME_FORMAT)
        request.session["start_double_auth"] = start

    def check_chrono(self, request, max_minutes=10):
        if "start_double_auth" not in request.session:
            messages.error(request, "Authentification requise.")
            return False
        start = datetime.strptime(request.session["start_double_auth"], DATETIME_FORMAT)
        ellapse = start - datetime.now()
        if ellapse > timedelta(minutes=max_minutes):
            messages.error(request, "Délais pour s'authentifier expiré.")
            return False
        return True

    def dispatch(self, request, *args, **kwargs):
        if not self.check_chrono(request, max_minutes=10):
            return HttpResponseRedirect(reverse("user:login"))
        return super().dispatch(request, *args, **kwargs)


class LoginView2(LoginView):
    redirect_authenticated_user = True
    template_name = "user/login.html"

    def form_valid(self, form):
        """Security check complete. Log the user in."""
        ChronoMixin.start_chrono(self.request)
        self.request.session["user_id"] = form.get_user().id
        return HttpResponseRedirect(reverse("user:double_auth_send"))

    def form_invalid(self, form):
        messages.error(self.request, "E-mail ou mot de passe incorrect.")
        return self.render_to_response(self.get_context_data(form=form))

    def get_success_url(self) -> str:
        if self.request.user.is_staff:
            return reverse("account:list")
        return reverse_lazy("home:home")


class SendDoubleAuthCodeView(ChronoMixin, RedirectView):
    pattern_name = "user:double_auth_check"

    def get(self, request, *args, **kwargs):
        user = User.objects.get(pk=self.request.session["user_id"])
        user.send_double_auth_code()
        messages.info(self.request, "Un code vous a été envoyé par e-mail.")
        return super().get(request, *args, **kwargs)


class GetSuccessUrlMixin:  # pylint: disable=R0903
    def get_success_url(self) -> str:
        if self.request.user.is_staff:  # type: ignore
            return reverse("account:list")
        return super().get_success_url()  # type: ignore


class CheckDoubleAuthCodeView(ChronoMixin, GetSuccessUrlMixin, FormView):
    template_name = "user/double_auth_code.html"
    form_class = DoubleAuthCodeForm
    success_url = reverse_lazy("home:home")

    def get_success_url(self) -> str:
        if self.request.user.must_change_pw:  # type: ignore
            return reverse("user:change_pw")
        return super().get_success_url()

    def form_valid(self, form):
        # check that the code is correct
        user = User.objects.get(id=self.request.session["user_id"])

        try:
            if user.check_double_auth_code(form.cleaned_data["code"]):
                if user.invitation_status == User.InvitationStatus.INVITED:
                    user.invitation_status = User.InvitationStatus.MEMBER
                    user.save(update_fields=["invitation_status"])

                user.reset_double_auth_code()
                auth_login(self.request, user)
                return HttpResponseRedirect(self.get_success_url())

            messages.error(self.request, "Code incorrect.")
        except CodeExpiredException:
            messages.error(self.request, "Code expiré, il n'est valable que 2 heures.")
        return self.render_to_response(self.get_context_data(form=form))


class PasswordChangeView2(RedirectURLMixin, GetSuccessUrlMixin, PasswordChangeView):
    template_name = "user/change_pw.html"
    next_page = reverse_lazy("home:home")

    def get_context_data(self, **kwargs):
        kwargs["extend_name"] = "index_unauthenticated.html"
        if self.request.GET.get("next", None):
            kwargs["extend_name"] = "index.html"
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        self.request.user.must_change_pw = False
        self.request.user.save(update_fields=["must_change_pw"])
        return super().form_valid(form)


class ProfileView(LoginRequiredMixin, UpdateView):
    model = User
    fields = (
        "first_name",
        "last_name",
        "email",
        "phone",
        "service",
        "function",
    )
    template_name = "user/profile.html"
    success_url = reverse_lazy("user:profile")

    def get_object(self, queryset=None):
        return self.request.user

    def dispatch(self, request, *args, **kwargs):
        self.kwargs["pk"] = request.user.id
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        messages.success(self.request, "Profil mis à jour.")
        return super().form_valid(form)


class DeactivateView(LoginRequiredMixin, RedirectView):
    pattern_name = "user:logout"

    def get(self, request, *args, **kwargs):
        self.request.user.is_active = False
        self.request.user.save()
        messages.success(
            self.request,
            "Votre compte a été désactivé, vous ne pouvez plus vous connecter.",
        )
        return super().get(request, *args, **kwargs)
