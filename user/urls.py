from django.contrib.auth import views as auth_views
from django.urls import path

from user import views

app_name = "user"


urlpatterns = [
    path("", views.ProfileView.as_view(), name="profile"),
    path("connexion", views.LoginView2.as_view(), name="login"),
    path("déconnexion", auth_views.LogoutView.as_view(), name="logout"),
    path(
        "double-authentification/envoyer-un-code",
        views.SendDoubleAuthCodeView.as_view(),
        name="double_auth_send",
    ),
    path(
        "double-authentification/vérifier-le-code",
        views.CheckDoubleAuthCodeView.as_view(),
        name="double_auth_check",
    ),
    path("changer-mot-de-passe", views.PasswordChangeView2.as_view(), name="change_pw"),
    path("désactiver", views.DeactivateView.as_view(), name="deactivate"),
]
